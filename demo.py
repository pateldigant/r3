
# coding: utf-8

# In[6]:


from matplotlib import pyplot as plt
import cv2 # used for resize. if you dont have it, use anything else
import numpy as np
from skimage import data, color
from skimage.transform import rescale, resize, downscale_local_mean



import os
from uuid import uuid4
import cv2
from flask import Flask, request, render_template, send_from_directory

__author__ = 'shubham'

app = Flask(__name__)

# app = Flask(__name__, static_folder="images")

APP_ROOT = os.path.dirname(os.path.abspath(__file__))






from model import Deeplabv3
deeplab_model = Deeplabv3()


# In[8]:

def predict(img,x):
	#img = plt.imread(destination)
	b, v, _ = img.shape
	ratio = 512. / np.max([b,v])

	if b>=v:
	    w=b
	    h=v
	else:
	    w=v
	    h=b


	resized = cv2.resize(img,(int(ratio*w),int(ratio*h)))
	resized = resized / 127.5 - 1.
	#print(resized)
	pad_x = int(512 - resized.shape[0])
	resized2 = np.pad(resized,((0,pad_x),(0,0),(0,0)),mode='constant')
	res = deeplab_model.predict(np.expand_dims(resized2,0))
	#print(resized2)
	labels = np.argmax(res.squeeze(),-1)
	m=labels[:-pad_x]
	k=np.copy(img)
	blur = cv2.blur(img,(13,13))
	#h=cv2.cvtColor(k,cv2.COLOR_RGB2GRAY)
	#added_image = cv2.addWeighted(img,0.4,image_resized,0.1,0)
	image_resized = resize(m,(b,v))
	#print(np.max(image_resized))

	image_resized[image_resized>=1.0805133673525319e-18]=255
	r=image_resized//255
	#print(r)
	blur[r==1]=0
	k[r==0]=0
	final=blur+k
	final_final=cv2.cvtColor(final,cv2.COLOR_BGR2RGB)
	#y=h+image_resized
	#print(image_resized)
	#blur[image_resized<1]=1
	#new=cv2.resize(,(1000,800),interpolation = cv2.INTER_AREA)
	#added_image = cv2.addWeighted(img,0.4,image_resized,0.1,0)
	cv2.imwrite(x,final_final)





img = plt.imread("images/image5.jpg")
predict(img,'abcdsf.jpg')


@app.route('/')
def index():
    return render_template('upload.html')


@app.route('/upload', methods=['POST'])
def upload():
    target = os.path.join(APP_ROOT, 'images/')

    # target = os.path.join(APP_ROOT, 'static/')

    print (target)
    if not os.path.isdir(target):
        os.mkdir(target)
    else:
        print ("Couldn't create upload directory: {}".format(target))
    print (request.files.getlist('file'))
    for upload in request.files.getlist('file'):
        print (upload)
        print ('{} is the file name'.format(upload.filename))
        filename = upload.filename
        destination = '/'.join([target, filename])
        print ('Accept incoming file:', filename)
        print ('Save it to:', destination)
        upload.save(destination)
        img = plt.imread(destination)
        #blur = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        x = 'proc/' + filename
        predict(img,x)
        #cv2.imwrite(x, blur)

    # return send_from_directory("images", filename, as_attachment=True)

    return render_template('complete_display_image.html', image_name=filename)


@app.route('/upload/<filename>')
def send_image(filename):
    return send_from_directory('proc', filename)

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5000, debug=False)









