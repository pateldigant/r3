#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
from uuid import uuid4
import cv2
from flask import Flask, request, render_template, send_from_directory

__author__ = 'ibininja'

app = Flask(__name__)

# app = Flask(__name__, static_folder="images")

APP_ROOT = os.path.dirname(os.path.abspath(__file__))


@app.route('/')
def index():
    return render_template('upload.html')


@app.route('/upload', methods=['POST'])
def upload():
    target = os.path.join(APP_ROOT, 'images/')

    # target = os.path.join(APP_ROOT, 'static/')

    print (target)
    if not os.path.isdir(target):
        os.mkdir(target)
    else:
        print ("Couldn't create upload directory: {}".format(target))
    print (request.files.getlist('file'))
    for upload in request.files.getlist('file'):
        print (upload)
        print ('{} is the file name'.format(upload.filename))
        filename = upload.filename
        destination = '/'.join([target, filename])
        print ('Accept incoming file:', filename)
        print ('Save it to:', destination)
        upload.save(destination)
        img = cv2.imread(destination)
        blur = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        x = 'proc/' + filename
        cv2.imwrite(x, blur)

    # return send_from_directory("images", filename, as_attachment=True)

    return render_template('complete_display_image.html', image_name=filename)


@app.route('/upload/<filename>')
def send_image(filename):
    return send_from_directory('proc', filename)

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=4555, debug=False)


			
